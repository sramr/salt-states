{% set token = salt['pillar.get']('dlmaster:providers:digitalocean:personal_access_token', 'invalid') %}

{% if token == 'invalid' %}
{% do salt.test.exception("Whoops! Check your pillar data! The token from 'dlmaster:providers:digitalocean:personal_access_token' has gone missing!") %}
{% endif %}

{% from "orchestration/ecorp/map.jinja" import servers,disks,volsize,fstype %}

{% for minionid in servers %}
{% if minionid.startswith('minio') is sameas true %}

spawn-{{ minionid }}:
  salt.runner:
    - name: state.orchestrate
    - mods: orchestration.spawn-vm
    - pillar:
        serverid: {{ minionid }}

    {% for disk in disks %}
volume-{{ disk }}-created-and-attached-to-{{ minionid }}:
  cmd.run:
    - name: /root/scripts/providers/doscript.sh {{ token }} createBlockStorageVolumeAndAttachItUsingDropletName {{ volsize }} {{ minionid }}-{{ disk }} {{ fstype }} {{ minionid  }}
    {% endfor %}

{% endif %}
{% endfor %}

