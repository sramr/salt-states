{% from "orchestration/ecorp/map.jinja" import servers %}

{% for minionid in servers %}
spawn-{{ minionid }}:
  salt.runner:
    - name: state.orchestrate
    - mods: orchestration.spawn-vm
    - pillar:
        serverid: {{ minionid }}

state-applied-{{ minionid }}:
  salt.state:
    - tgt: {{ minionid }}
    - sls:
{% if minionid.startswith('nc') is sameas true %}
        - nextcloud-node
{% elif minionid.startswith('db') is sameas true %}
        - galera-node
{% else %}
        - common
{% endif %}
    - retry:
        attempts: 10
        until: True
        interval: 5
        splay: 5
    - require:
      - spawn-{{ minionid }}
{% if minionid.startswith('nc') is sameas true %}
      - db-created
    - order: last
{% endif %}
{% endfor %}


cluster-initialized:
  salt.function:
    - tgt: dbnode1
    - name: cmd.run
    - arg:
      - galera_new_cluster
    - kwarg:
       shell: /bin/bash
    - require:
{% for minionid in servers %}
{% if minionid.startswith('db') is sameas true %}
      - state-applied-{{ minionid }}
{% endif %}
{% endfor %}

dbnodes-started:
  salt.function:
    - tgt:
{% for minionid in servers %}
{% if minionid.startswith('db') is sameas true %}
      - {{ minionid }}
{% endif %}
{% endfor %}
    - name: cmd.run
    - arg:
      - systemctl enable --now mariadb || true
    - kwarg:
       shell: /bin/bash
    - require:
      - cluster-initialized

db-created:
  salt.function:
    - tgt: dbnode1
    - name: cmd.run
    - arg:
      - mysql -u root -e 'CREATE DATABASE nc;' && mysql -u root -e "CREATE USER 'ncuser'@'%' IDENTIFIED BY 'ncuser';" && mysql -u root -e  "GRANT ALL PRIVILEGES ON nc.* TO 'ncuser'@'%';"
    - kwarg:
       shell: /bin/bash
    - require:
      - dbnodes-started