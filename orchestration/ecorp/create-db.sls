# TBD this should utilize salt functions (see comments in nextcloud-node) instead of cmd.run
db-created:
  salt.function:
    - tgt: db*1
    - tgt_type: glob
    - name: cmd.run
    - arg:
      - mysql -u root -e 'CREATE DATABASE nc;' && mysql -u root -e "CREATE USER 'ncuser'@'%' IDENTIFIED BY 'ncuser';" && mysql -u root -e  "GRANT ALL PRIVILEGES ON nc.* TO 'ncuser'@'%';"
    - kwarg:
       shell: /bin/bash