{% from "orchestration/ecorp/map.jinja" import servers %}

{% for minionid in servers %}
{% if minionid.startswith('ha') is sameas true %}

spawn-{{ minionid }}:
  salt.runner:
    - name: state.orchestrate
    - mods: orchestration.spawn-vm
    - pillar:
        serverid: {{ minionid }}

state-applied-{{ minionid }}:
  salt.state:
    - tgt: {{ minionid }}
    - sls:
        - haproxy-node
    - require:
      - spawn-{{ minionid }}
{% endif %}
{% endfor %}

