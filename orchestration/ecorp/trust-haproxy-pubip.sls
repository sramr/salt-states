{%- set haproxypubip = salt["cmd.run"]("bash /root/scripts/gethaproxypubip.sh") -%}
{% from "orchestration/ecorp/map.jinja" import servers %}

haproxy-trusted_domains-configured:
  salt.function:
    - tgt:
{% for minionid in servers %}
{% if minionid.startswith('nc') is sameas true %}
      - {{ minionid }}
{% endif %}
{% endfor %}
    - tgt_type: list
    - name: cmd.run
    - arg:
      - sudo -u www-data php /var/www/nextcloud/occ config:system:set trusted_domains 0 --update-only --type=string --value={{ haproxypubip }}
    - kwargs:
      - shell: /bin/bash
