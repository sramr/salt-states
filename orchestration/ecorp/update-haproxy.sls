{%- set ncservers = salt["cmd.run"]("bash /root/scripts/extconf.sh").split("\n") -%}

{% for server in ncservers %}
haproxy-nodes-configured-{{ server }}:
  salt.function:
    - tgt: haproxy
    - name: file.append
    - arg:
      - /etc/haproxy/haproxy.cfg
      - '{{ server }}'
    - require_in:
      - haproxy-restarted
    - unless: grep "{{ server }}" /etc/haproxy/haproxy.cfg
{%- endfor %}

haproxy-restarted:
  salt.function:
    - tgt: haproxy
    - name: cmd.run
    - arg:
      - 'systemctl restart haproxy'
    - kwarg:
       shell: /bin/bash