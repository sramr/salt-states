{% from "orchestration/ecorp/map.jinja" import servers %}

{% for minionid in servers %}
{% if minionid.startswith('db') is sameas true %}

spawn-{{ minionid }}:
  salt.runner:
    - name: state.orchestrate
    - mods: orchestration.spawn-vm
    - pillar:
        serverid: {{ minionid }}

state-applied-{{ minionid }}:
  salt.state:
    - tgt: {{ minionid }}
    - sls:
      - galera-node
    - require:
      - spawn-{{ minionid }}
{% endif %}
{% endfor %}

cluster-initialized:
  salt.function:
    - tgt: db*1
    - tgt_type: glob
    - name: cmd.run
    - arg:
      - galera_new_cluster
    - kwarg:
       shell: /bin/bash
    - require:
{% for minionid in servers %}
{% if minionid.startswith('db') is sameas true %}
      - state-applied-{{ minionid }}
{% endif %}
{% endfor %}

dbnodes-started:
  salt.function:
    - tgt: db*
    - tgt_type: glob
    - name: service.start
    - arg:
      - mariadb
    - require:
      - cluster-initialized

dbnodes-enabled:
  salt.function:
    - tgt: db*
    - tgt_type: glob
    - name: service.enable
    - arg:
      - mariadb
    - require:
      - dbnodes-started