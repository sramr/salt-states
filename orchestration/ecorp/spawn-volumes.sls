{% from "orchestration/ecorp/map.jinja" import servers %}
{% from "orchestration/ecorp/map.jinja" import disks %}

{%- set do_token = salt['pillar.get']('dlmaster:providers:digitalocean:personal_access_token', '') %}
{%- set hostname = salt['grains.get']('host') -%}
{# {% set minionid = salt['cmd.run']('salt-cloud -y -a show_instance salt-master | grep -A10 "{{ hostname }}" | grep -A1 "id" | tail -n1 | xargs')%} #}

volume-directory-created:
  file.directory:
    - name: /root/volume
    - user: root
    - group: root
    - mode: 755
    - makedirs: True

minionid-file-created:
  file.managed:
      - name: /root/volume/minionid
      - content: ""
      - user: root
      - group: root
      - mode: 644
      - replace: false

minion-id-collected:
  cmd.run:
    - name: >
        salt-cloud -y -a show_instance salt-master 
        | grep -A10 "{{ hostname }}" 
        | grep -A1 "id" 
        | tail -n1 
        | xargs 
        | awk '{print "export MINIONID="$1}'
        >> /root/volume/minionid

minio-id-present:
  cmd.run:
    - name: cat /root/volume/minionid