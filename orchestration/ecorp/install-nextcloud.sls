{% from "orchestration/ecorp/map.jinja" import servers %}


spawn-first-nc-node:
  salt.runner:
    - name: state.orchestrate
    - mods: orchestration.spawn-vm
    - pillar:
        serverid: ncnode1

state-applied-first-nc-node:
  salt.state:
    - tgt: ncnode1
    - sls:
      - nextcloud-node
    - require:
      - spawn-first-nc-node

{% for minionid in servers %}
{% if (minionid.startswith('nc') is sameas true) and (minionid != 'ncnode1') %}
spawn-{{ minionid }}:
  salt.runner:
    - name: state.orchestrate
    - mods: orchestration.spawn-vm
    - pillar:
        serverid: {{ minionid }}

state-applied-{{ minionid }}:
  salt.state:
    - tgt: {{ minionid }}
    - sls:
      - nextcloud-node
    - require:
      - spawn-{{ minionid }}
{% endif %}
{% endfor %}