{% from "orchestration/ecorp/map.jinja" import servers %}

spawn-minio-nodes:
  salt.runner:
    - name: state.orchestrate
    - mods: orchestration.ecorp.spawn-minio-nodes

hostsfiles-placed:
  salt.function:
    - tgt: minio*
    - tgt_type: glob
    - name: file.append
    - arg:
        - /etc/hosts
    - kwarg:
        text: {{ salt["cmd.shell"]("bash /root/scripts/get_wghosts.sh") | yaml  }}


  {% for minionid in servers %}
  {% if minionid.startswith('minio') is sameas true %}

state-applied-{{ minionid }}:
  salt.state:
    - tgt: {{ minionid }}
    - sls:
        - minio-node
  {% endif %}
  {% endfor %}