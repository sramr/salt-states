{% from "orchestration/ecorp/map.jinja" import servers %}

{% for minionid in servers %}
spawn-{{ minionid }}:
  salt.runner:
    - name: state.orchestrate
    - mods: orchestration.spawn-vm
    - pillar:
        serverid: {{ minionid }}
{% endfor %}

{% for minionid in servers %}
state-applied-{{ minionid }}:
  salt.state:
    - tgt: {{ minionid }}
    - sls:
{% if minionid.startswith('db') is sameas true %}
        - galera-node
{% elif minionid.startswith('haproxy') is sameas true %}
        - haproxy-node
{% else %}
        - common
{% endif %}
    - retry:
        attempts: 10
        until: True
        interval: 5
        splay: 5
    - require:
      - spawn-{{ minionid }}
{% endfor %}


cluster-initialized:
  salt.function:
    - tgt: dbnode1
    - name: cmd.run
    - arg:
      - galera_new_cluster
    - kwarg:
       shell: /bin/bash
    - require:
{% for minionid in servers %}
{% if minionid.startswith('db') is sameas true %}
      - state-applied-{{ minionid }}
{% endif %}
{% endfor %}

dbnodes-started:
  salt.function:
    - tgt:
{% for minionid in servers %}
{% if minionid.startswith('db') is sameas true %}
      - {{ minionid }}
{% endif %}
{% endfor %}
    - tgt_type: list
    - name: service.start
    - arg:
      - mariadb
    - require:
      - cluster-initialized

dbnodes-enabled:
  salt.function:
    - tgt:
{% for minionid in servers %}
{% if minionid.startswith('db') is sameas true %}
      - {{ minionid }}
{% endif %}
{% endfor %}
    - tgt_type: list
    - name: service.enable
    - arg:
      - mariadb
    - require:
      - dbnodes-started


# TBD this should utilize salt functions (see comments in nextcloud-node) instead of cmd.run
db-created:
  salt.function:
    - tgt: dbnode1
    - name: cmd.run
    - arg:
      - mysql -u root -e 'CREATE DATABASE nc;' && mysql -u root -e "CREATE USER 'ncuser'@'%' IDENTIFIED BY 'ncuser';" && mysql -u root -e  "GRANT ALL PRIVILEGES ON nc.* TO 'ncuser'@'%';"
    - kwarg:
       shell: /bin/bash
    - require:
      - dbnodes-enabled

# this is incredibly strange that implicit ordering requires to separate ncnodes (to be debugged, compare with keep-to-...sls)
{% for minionid in servers %}
{% if minionid.startswith('nc') is sameas true %}
finalstate-applied-{{ minionid }}:
  salt.state:
    - tgt: {{ minionid }}
    - sls:
        - nextcloud-node
    - require:
      - spawn-{{ minionid }}
      - db-created
    - require_in:
      - haproxy-configured
{% endif %}
{% endfor %}

haproxy-configured:
  salt.runner:
    - name: state.orchestrate
    - mods: orchestration.ecorp.update-haproxy

haproxy-trusted:
  salt.runner:
    - name: state.orchestrate
    - mods: orchestration.ecorp.trust-haproxy-pubip
    - require:
      - haproxy-configured