galera-cluster-spawned:
  salt.runner:
    - name: state.orchestrate
    - mods: orchestration.ecorp.spawn-galera-cluster

db-created:
  salt.runner:
    - name: state.orchestrate
    - mods: orchestration.ecorp.create-db
    - require:
      - galera-cluster-spawned

minio-created:
  salt.runner:
    - name: state.orchestrate
    - mods: orchestration.ecorp.spawn-minio

nc-installed:
  salt.runner:
    - name: state.orchestrate
    - mods: orchestration.install-nextcloud
    - require:
      - db-created
      - minio-created

haproxy-installed:
  salt.runner:
    - name: state.orchestrate
    - mods: orchestration.spawn-haproxy
    - require:
      - nc-installed

haproxy-configured:
  salt.runner:
    - name: state.orchestrate
    - mods: orchestration.ecorp.update-haproxy
    - require:
      - haproxy-installed

haproxy-trusted:
  salt.runner:
    - name: state.orchestrate
    - mods: orchestration.ecorp.trust-haproxy-pubip
    - require:
      - haproxy-configured