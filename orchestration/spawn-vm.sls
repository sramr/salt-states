{%- set serverid = salt['pillar.get']('serverid', 'Unknown') -%}
{%- set nextip = salt["cmd.run"]("bash /root/scripts/combined_nextip.sh") -%}
include:
 - dlmaster

wg-server-present:
  cloud.profile:
    - name: wg-server
    - profile: wireguard-server-do
    - require:
      - sls: dlmaster

master-peered-with-wgserver:
  cmd.run:
    - name: bash /root/scripts/peer-master-with-wgserver.sh
    - unless: /root/scripts/peered-ok.sh
    - retry:
        attempts: 10
        until: True
        interval: 5
        splay: 5
    - require:
      - wg-server-present

initial-ping-done:
  cmd.run:
    - name: ping -c3 192.168.11.1 && ping -c3 192.168.11.2
    - onchanges:
      - master-peered-with-wgserver

wgserver-configured:
  salt.state:
    - tgt: wg-server
    - sls:
        - wgserver
    - retry:
        attempts: 10
        until: True
        interval: 5
        splay: 5
    - require:
      - master-peered-with-wgserver
      - initial-ping-done

common-state-applied:
  salt.state:
    - tgt: '*'
    - sls:
        - common
    - retry:
        attempts: 10
        until: True
        interval: 5
        splay: 5
    - require:
      - wgserver-configured
      - initial-ping-done

flesh-server-present:
  cloud.profile:
    - name: {{ serverid }}
    - profile: flesh-server-do
    - unless: /root/scripts/instance_exists.sh {{ serverid }}
    - require:
      - common-state-applied

flesh-instance-peered-with-wgserver:
  cmd.run:
    - name: bash /root/scripts/peer-fleshserver-with-wgserver.sh {{ serverid }}
    - onchanges:
      -  flesh-server-present

common-state-flesh-server-applied:
  salt.state:
    - tgt: {{ serverid }}
    - sls:
        - common
        - common-fleshserver
    - retry:
        attempts: 10
        until: True
        interval: 5
        splay: 5
    - require:
      - flesh-instance-peered-with-wgserver

flesh-server-minion-moved:
  cmd.run:
{% if nextip == '' %}
    - name: ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null 192.168.11.3 "bash /root/scripts/move-minion.sh"
{% else %}
    - name: ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null {{ nextip }} "bash /root/scripts/move-minion.sh"
{% endif %}
    - require:
      - common-state-flesh-server-applied