masterless-minion-config-placed:
  file.managed:
    - name: /etc/salt/minion.d/00_masterless.conf
    - source: salt://files/00_masterless.conf
    - mode: 750
    - makedirs: true
    - require:
      - flesh-server-minion-moved

flesh-server-minion-moved:
  cmd.run:
    - name: bash /root/scripts/move-minion.sh
    - unless: test -f /devlab/etc/salt/minion

masterless-minion-running:
  service.running:
    - name: devlab-salt-minion
    - enable: true
    - require:
      - flesh-server-minion-moved
    - watch:
      - masterless-minion-config-placed
