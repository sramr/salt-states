{%- set kernelvers = salt["cmd.run"]("uname -r") -%}
base-packages-installed:
  pkg.installed:
    - pkgs:
      - rsync
      - software-properties-common
