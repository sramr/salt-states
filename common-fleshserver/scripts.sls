move-minion-script placed:
  file.managed:
    - name: /root/scripts/move-minion.sh
    - source: salt://files/move-minion.sh
    - mode: 750
    - makedirs: true
