sshd-config-duplicated:
  file.copy:
    - name: /etc/ssh/sshd_config_dlmaster
    - source: /etc/ssh/sshd_config
    - preserve: True
    - require:
      - sshd-config-iface-tweaked

sshd-systemdunit-duplicated:
  file.copy:
    - name: /lib/systemd/system/sshd-dlmaster.service
    - source: /lib/systemd/system/ssh.service
    - preserve: True

sshd-config-port-tweaked:
  file.replace:
    - name: /etc/ssh/sshd_config_dlmaster
    - pattern: '#Port 22'
    - repl: Port 2222
    - require:
      - sshd-config-duplicated

sshd-config-privsep-tweaked:
  file.append:
    - name: /etc/ssh/sshd_config_dlmaster
    - text: UsePrivilegeSeparation no
    - require:
      - sshd-config-duplicated

sshd-config-iface-tweaked:
  file.replace:
    - name: /etc/ssh/sshd_config
    - pattern: '#ListenAddress 0.0.0.0'
    - repl: ListenAddress {{ salt['grains.get']('ip4_interfaces:wg0')|first }}

sshd-running:
  service.running:
    - name: sshd
    - enable: true
    - watch:
      - sshd-config-iface-tweaked

sshd-dlmaster-running:
  service.running:
    - name: sshd-dlmaster
    - enable: true
    - require:
      - sshd-systemdunit-alias-tweaked
      - sshd-systemdunit-config-file-tweaked
    - watch:
      - sshd-config-port-tweaked
      - sshd-config-privsep-tweaked

sshd-systemdunit-config-file-tweaked:
  file.replace:
    - name: /lib/systemd/system/sshd-dlmaster.service
    - pattern: SSHD_OPTS$
    - repl: SSHD_OPTS -f /etc/ssh/sshd_config_dlmaster
    - require:
      - sshd-systemdunit-duplicated

sshd-systemdunit-alias-tweaked:
  file.replace:
    - name: /lib/systemd/system/sshd-dlmaster.service
    - pattern: sshd.service
    - repl: sshd-dlmaster.service
    - require:
      - sshd-systemdunit-duplicated

