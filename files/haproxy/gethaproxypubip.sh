#!/bin/bash
salt haproxy grains.get ip4_interfaces:eth0 | grep -v "\- 10\." | tail -n1 | awk '{ print $NF }'