#!/usr/bin/env bash
salt-key -L | grep ^ncnode | while read srv; do
IP=$(salt ${srv} grains.get ip4_interfaces:wg0 | tail -n1 | awk  '{ print $NF }')
echo "    server $srv $IP:80 check cookie ${srv}Cookie"
:;done