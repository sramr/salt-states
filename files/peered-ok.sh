#!/usr/bin/env bash
wg | grep -q "endpoint: $(salt-call cloud.get_instance wg-server | grep -A1 ip_address | tail -n1 | awk '{ print $NF }')"
