#!/usr/bin/env bash
salt \* cmd.run 'bash /root/scripts/nextip.sh' 2>&1 | grep -v : | grep -P '([0-9]{1,3}\.){3}[0-9]{1,3}(\/([0-9]|[1-2][0-9]|3[0-2]))?' | awk '{ print $NF }' | sort -ur | head -n1 | sed 's@/.*$@@g'
