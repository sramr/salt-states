#!/bin/bash

# Parameters 
# 1 -> api token
# 2 -> function to call
# 3 to n -> parameters to function | see function doc for details

if [ $# -lt 3 ]
then
 echo Parameter error
 exit 1
fi

# This call is used to check authorization and is theoretically only requied for fucntion getDropletIdByName
RAW_DROPLETS=$(curl -sX GET -H "Content-Type: application/json" -H "Authorization: Bearer $1" "https://api.digitalocean.com/v2/droplets")
echo "$RAW_DROPLETS" | grep -q Unauthorized && (echo Authorization error && exit 1)

# 3 -> droplet name to search for
# returns -> droplet id if found
function getDropletIdByName {
    SIZE=$(echo $RAW_DROPLETS | jq '.["meta"].total')
    for ((ctr=0; ctr<$SIZE;ctr=ctr+1)); do echo "$RAW_DROPLETS" | jq '{ name: .["droplets"]|.['$ctr'].name, id: .["droplets"]|.['$ctr'].id }'; done | grep -A1 "$3" | tail -n1 | awk -F: '{ print $NF }' | awk '{ print $NF }'
}

# 3 -> size in gb
# 4 -> name/description/label (all set to the same value)
# 5 -> region
# 6 -> fstype
# returns -> volume id
function createBlockStorageVolume {
	curl -sX POST -H "Content-Type: application/json" -H "Authorization: Bearer $1" -d '{"size_gigabytes":'$3', "name": "'$4'", "description": "'$4'", "region": "'$5'", "filesystem_type": "'$6'", "filesystem_label": "'$4'"}' "https://api.digitalocean.com/v2/volumes" | jq '.volume.id' | sed 's@^.@@g' | sed 's@.$@@g'
}

# 3 -> droplet id to attach to
# 4 -> droplet location
# 5 -> volume id
# returns -> api status
function attachBlockStorageVolumeToDropletUsingVolumeIdAndDropletId {
	RAW_OUTPUT=$(curl -sX POST -H "Content-Type: application/json" -H "Authorization: Bearer $1" -d '{"type": "attach", "droplet_id": '$3', "region": "'$4'"}' "https://api.digitalocean.com/v2/volumes/$5/actions")
	echo "$RAW_OUTPUT" | jq '.action.status' | sed 's@^.@@g' | sed 's@.$@@g'
}

# 3 -> droplet id to return region for
# returns -> droplet region slug if found
function getDropletRegionByDropletId {
	RAW_DROPLET=$(curl -sX GET -H "Content-Type: application/json" -H "Authorization: Bearer $1" "https://api.digitalocean.com/v2/droplets/$3")
	echo "$RAW_DROPLET" | jq '{ name: .[].name, region: .[].region.slug }' | grep region | tail -n1 | awk -F: '{ print $NF }' | awk '{ print $NF }' | sed 's@^.@@g' | sed 's@.$@@g'
}

# 3 -> droplet name to return region for
# returns -> droplet region slug if found
function getDropletRegionByDropletName {
	DROPLET_ID=$(getDropletIdByName $1 "dummy" $3)
	getDropletRegionByDropletId $1 "dummy" $DROPLET_ID
}

# 3 -> size in gb
# 4 -> name/description/label (all set to the same value)
# 5 -> fstype
# 6 -> droplet id to attach to
# returns -> volume id of new volume and api status of attachment process
function createBlockStorageVolumeAndAttachItUsingDropletId {
	DROPLET_REGION=$(getDropletRegionByDropletId $1 "dummy" $6)
	VOL_ID=$(createBlockStorageVolume $1 "dummy" $3 $4 $DROPLET_REGION $5)
	echo "Volume $4 with ID $VOL_ID created"
	attachBlockStorageVolumeToDropletUsingVolumeIdAndDropletId $1 "dummy" $6 $DROPLET_REGION $VOL_ID
}

# 3 -> size in gb
# 4 -> name/description/label (all set to the same value)
# 5 -> fstype
# 6 -> droplet name to attach to
# returns -> volume id of new volume and api status of attachment process
function createBlockStorageVolumeAndAttachItUsingDropletName {
	DROPLET_ID=$(getDropletIdByName $1 "dummy" $6)
	createBlockStorageVolumeAndAttachItUsingDropletId $1 "dummy" $3 $4 $5 $DROPLET_ID
}

$2 "$@"
