#!/usr/bin/env bash
systemctl stop salt-minion
mkdir -p /devlab/etc /devlab/var/cache /devlab/var/run /devlab/var/log
mv /etc/salt/ /devlab/etc
mv /var/cache/salt/ /devlab/var/cache
mv /var/log/salt/ /devlab/var/log
mv /var/run/salt/ /devlab/var/run
cp -p /lib/systemd/system/salt-minion.service /lib/systemd/system/devlab-salt-minion.service
sed -i 's@ExecStart=/usr/bin/salt-minion@ExecStart=/usr/bin/salt-minion -c /devlab/etc/salt@g' /lib/systemd/system/devlab-salt-minion.service
echo "root_dir: /devlab" > /devlab/etc/salt/minion.d/root_dir.conf
systemctl daemon-reload
systemctl enable --now devlab-salt-minion
mkdir -p /etc/salt/minion.d/ /srv/salt
echo -e "file_client: local\nfile_roots:\n  base:\n    - /srv/salt" > /etc/salt/minion.d/00_masterless.conf
systemctl enable --now salt-minion
systemctl restart salt-minion

