#!/bin/bash
salt \* grains.get ip4_interfaces:wg0 --out=json | jq '.' -c | sed 's@["{}\[]@@g' | sed 's@.$@@g' | awk -F: '{ print $2"     "$1 }'
