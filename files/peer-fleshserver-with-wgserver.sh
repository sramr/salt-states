#!/usr/bin/env bash

FLESH_SERVER_ID=$1
WG_SERVER_USERNAME=root
LISTENPORT=54321
FLESH_SERVER_USERNAME=root

IP_WG_SERVER=$(salt-call cloud.get_instance wg-server | grep -A1 ip_address | tail -n1 | awk '{ print $NF }')
IP_FLESH_SERVER=$(salt-call cloud.get_instance ${FLESH_SERVER_ID} | grep -A1 ip_address | tail -n1 | awk '{ print $NF }')

# if flesh server is already peered to wireguard server -> exit
ssh -q -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ${FLESH_SERVER_USERNAME}@${IP_FLESH_SERVER}  "wg show wg0 | grep endpoint: | grep -q ${IP_WG_SERVER}" && exit 0

NEXT_IP=$(bash /root/scripts/combined_nextip.sh)

# fix flesh server by moving it from template subnet IP to its real IP
ssh -q -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ${FLESH_SERVER_USERNAME}@${IP_FLESH_SERVER}  "systemctl stop wg-quick@wg0 && sed -i 's@192.168.12.250@${NEXT_IP}@g' /etc/wireguard/wg0.conf && systemctl start wg-quick@wg0"

PUBKEY_WG_SERVER=$(ssh -q -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ${WG_SERVER_USERNAME}@${IP_WG_SERVER} "wg | grep -A1 interface: | tail -n1" | awk -F: '{ print $NF }' | awk '{ print $NF }')
PUBKEY_FLESH_SERVER=$(ssh -q -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ${FLESH_SERVER_USERNAME}@${IP_FLESH_SERVER} "wg | grep -A1 interface: | tail -n1" | awk -F: '{ print $NF }' | awk '{ print $NF }')

ssh -q -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ${FLESH_SERVER_USERNAME}@${IP_FLESH_SERVER}  "wg-quick down wg0; wg-quick up wg0 && wg set wg0 peer ${PUBKEY_WG_SERVER} endpoint ${IP_WG_SERVER}:${LISTENPORT} allowed-ips 192.168.11.0/24"

ssh -q -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ${WG_SERVER_USERNAME}@${IP_WG_SERVER} "wg-quick down wg0; wg-quick up wg0 && wg set wg0 peer ${PUBKEY_FLESH_SERVER} allowed-ips ${NEXT_IP}/32"
