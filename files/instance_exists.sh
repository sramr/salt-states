#!/usr/bin/env bash
salt-cloud -F | grep -A1 name: | tail -n1 | awk '{ print $NF }' | grep -q "^$1\$"
