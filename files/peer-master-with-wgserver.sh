#!/usr/bin/env bash

WG_SERVER_USERNAME=root
LISTENPORT=54321
IP_WG_SERVER=$(salt-call cloud.get_instance wg-server | grep -A1 ip_address | tail -n1 | awk '{ print $NF }')

# if already peered to wireguard server -> exit
wg show wg0 | grep endpoint: | grep -q ${IP_WG_SERVER} && exit 0

PUBKEY_WG_SERVER=$(ssh -q -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ${WG_SERVER_USERNAME}@${IP_WG_SERVER} "wg | grep -A1 interface: | tail -n1" | awk -F: '{ print $NF }' | awk '{ print $NF }')
wg-quick down wg0
wg-quick up wg0
wg set wg0 peer ${PUBKEY_WG_SERVER} endpoint ${IP_WG_SERVER}:${LISTENPORT} allowed-ips 192.168.11.0/24
ssh -q -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ${WG_SERVER_USERNAME}@${IP_WG_SERVER} "wg-quick down wg0; wg-quick up wg0 && wg set wg0 peer $(cat /etc/wireguard/publickey) allowed-ips 192.168.11.2/32"
