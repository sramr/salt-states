#!/usr/bin/env bash
apt-get update
export DEBIAN_FRONTEND=noninteractive ;
apt install -y linux-image-virtual linux-headers-$(uname -r) software-properties-common
add-apt-repository -y ppa:wireguard/wireguard
apt-get update
apt install -y wireguard curl
apt -y autoremove
systemctl disable --now ufw
umask 077
wg genkey | tee /etc/wireguard/privatekey | wg pubkey > /etc/wireguard/publickey

#TBD is that required on clients?
sysctl -w net.ipv4.ip_forward=1
#TBD + make this persistent via base state

echo -n "[Interface]
Address = 192.168.12.250/24
SaveConfig = true
PrivateKey = " > /etc/wireguard/wg0.conf
cat /etc/wireguard/privatekey >> /etc/wireguard/wg0.conf

chmod 700 /etc/wireguard/wg0.conf
rm -f /etc/wireguard/privatekey
systemctl enable --now wg-quick@wg0
