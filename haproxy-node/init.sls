include:
  - haproxy-node.ospackages

haproxy-baseconfig-placed:
  file.append:
    - name: /etc/haproxy/haproxy.cfg
    - source: salt://files/haproxy/baseconf.conf
    - require:
      - haproxy-base-packages-installed

haproxy-service-running:
  service.running:
    - name: salt-minion
    - enable: true
    - reload: true