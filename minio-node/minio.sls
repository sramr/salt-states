{% set hostname = salt['grains.get']('host') %}
  {% set accesskey = salt['pillar.get']('dlmaster:cluster:ecloud:objectstore:accesskey', 'invalid') %}
  {% set secretkey = salt['pillar.get']('dlmaster:cluster:ecloud:objectstore:secretkey', 'invalid') %}
  {% from "orchestration/ecorp/map.jinja" import disks, accesskey, secretkey, options %}

minio-packages-installed:
  pkg.installed:
    - pkgs:
        - curl
        - git

minio-user-created:
  user.present:
    - name: minio
    - uid: 4000

minio-group-create:
  group.present:
    - name: minio
    - gid: 4001
    - addusers:
        - minio

printed-pillar-data:
  cmd.run:
    - name: echo '{{ disks }} {{ accesskey }} {{ secretkey}}'1

  {% for disk in disks %}
minio-disks-{{ disk }}-mounted:
  mount.mounted:
    - name: /mnt/data{{ disk }}
    - device: /dev/disk/by-id/scsi-0DO_Volume_{{ hostname }}-{{ disk }}
    - fstype: ext4
    - persist: True
    - mkmnt: True

minio-disk-{{ disk }}-permission-set:
  file.directory:
    - name: /mnt/data{{ disk}}
    - user: minio
    - group: minio
    - mode: 755
  {% endfor %}

minio-exec-configured:
  file.managed:
    - name: /usr/local/bin/minio
    - source: https://dl.min.io/server/minio/release/linux-amd64/minio
    - mode: 755
    - user: minio
    - group: minio
    - skip_verify: True
    - require:
        - minio-user-created

minio-env-file-configured:
  file.managed:
    - name: /etc/default/minio
    - source: salt://files/minio/minio.jinja
    - template: jinja
    - mode: 755
    - user: minio
    - group: minio
    - require:
        - minio-user-created

minio-service-file-configured:
  file.managed:
    - name: /etc/systemd/system/minio.service
    - source: salt://files/minio/minio.service
    - mode: 755
    - user: minio
    - group: minio
    - require:
        - minio-user-created

minio-service-running:
  service.running:
    - name: minio
    - enable: true
    - reload: true
    - require:
        - minio-exec-configured
        - minio-service-file-configured