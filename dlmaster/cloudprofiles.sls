{%- for profile, profiledata in salt['pillar.get']('dlmaster:profiles').items() %}
cloud-profile-{{ profile }}:
  file.serialize:
    - name: /etc/salt/cloud.profiles.d/{{ profile }}.conf
    - formatter: yaml
    - dataset:
        {{ profile }}: {{ profiledata|yaml }}
    - require:
      - dlmaster-master-configured
{%- endfor %}
