share-exported:
  nfs_export.present:
    - name: /srv/data/nfsshare
    - hosts: '192.168.11.0/24'
    - options:
      - 'rw'
      - 'sync'
      - 'no_subtree_check'
    - require:
      - base-packages-installed
      - nfsdir-present

nfsdir-present:
  file.directory:
    - name: /srv/data/nfsshare
    - makedirs: true
    - user: www-data
    - group: www-data
