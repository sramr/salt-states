getwghots-placed:
  file.managed:
    - name: /root/scripts/get_wghosts.sh
    - source: salt://files/get_wghosts.sh
    - makedirs: True
    - mode: 750
