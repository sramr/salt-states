dlmaster-running:
  service.running:
    - name: salt-master
    - enable: true
    - unmask: true
    - unless: ps -ef | grep [s]alt-master | wc -l | grep -q ^0$
    - require:
      - base-packages-installed
    - watch:
      - dlmaster-master-configured

dlmaster-minion-running:
  service.running:
    - name: salt-minion
    - enable: true
    - require:
      - base-packages-installed
      - masterless-config-removed
      - dlmaster-minion-configured
    - watch:
      - dlmaster-minion-configured

masterless-config-removed:
  file.absent:
    - name: /etc/salt/minion.d/00_masterless.conf

dlmaster-minion-configured:
  file.managed:
    - name: /etc/salt/minion.d/minion.conf
    - source: salt://files/minion.conf

ssh-keypair-root-generated:
  cmd.run:
    - name: ssh-keygen -q -N '' -f /root/.ssh/id_rsa
    - unless: test -f /root/.ssh/id_rsa

dlmaster-master-configured:
  file.managed:
    - name: /etc/salt/master.d/master.conf
    - source: salt://files/master.conf

dlmaster-wg-dependency-configured:
  file.managed:
    - name: /etc/systemd/system/salt-master.service.d/override.conf
    - source: salt://files/salt-master-wg-dependency.conf
    - makedirs: True
    - require:
      - wireguard-interface-running
      - dlmaster-running

dlmaster-instance-existence-script-placed:
  file.managed:
    - name: /root/scripts/instance_exists.sh
    - source: salt://files/instance_exists.sh
    - makedirs: True
    - mode: 750
    - require:
      - wireguard-interface-running
      - dlmaster-running

dlmaster-flesh-server-peering-script-placed:
  file.managed:
    - name: /root/scripts/peer-fleshserver-with-wgserver.sh
    - source: salt://files/peer-fleshserver-with-wgserver.sh
    - makedirs: True
    - mode: 750
    - require:
      - wireguard-interface-running
      - dlmaster-running

haproxy-script-placed:
  file.managed:
    - name: /root/scripts/extconf.sh
    - source: salt://files/haproxy/extendedconf.sh
    - makedirs: True
    - mode: 750

haproxyip-script-placed:
  file.managed:
    - name: /root/scripts/gethaproxypubip.sh
    - source: salt://files/haproxy/gethaproxypubip.sh
    - makedirs: True
    - mode: 750