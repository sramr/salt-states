doscript-placed:
  file.managed:
    - name: /root/scripts/providers/doscript.sh
    - source: salt://files/doscript.sh
    - makedirs: True
    - mode: 750
