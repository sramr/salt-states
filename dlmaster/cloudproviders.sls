{%- for provider, providerdata in salt['pillar.get']('dlmaster:providers').items() %}
cloud-provider-config-{{ provider }}:
  file.serialize:
    - name: /etc/salt/cloud.providers.d/{{ provider }}.conf
    - formatter: yaml
    - dataset:
        {{ provider }}: {{ providerdata|yaml }}
    - require:
      - dlmaster-master-configured
{%- endfor %}

gce-key-placed:
  file.managed:
    - name: /root/.gce/saltstack.json
    - source: salt://files/gce-key-saltstack-project.json
    - makedirs: True
    - mode: 400