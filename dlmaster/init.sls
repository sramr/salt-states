include:
  - dlmaster.ospackages
  - dlmaster.saltmaster
  - dlmaster.wireguardclient
  - dlmaster.cloudprofiles
  - dlmaster.cloudproviders
  - dlmaster.custombootstrap
  - dlmaster.saltapi
  - common
  - dlmaster.nfsexport
  - dlmaster.doscript
  - dlmaster.wghostsscript

# https://github.com/saltstack/salt/issues/10852
wtf:
  test.nop