wireguard-keys-generated:
  cmd.run:
    - name: wg genkey | tee /etc/wireguard/privatekey | wg pubkey > /etc/wireguard/publickey && chmod 400 /etc/wireguard/*key
    - unless: test -f /etc/wireguard/publickey

ip-forwarding-enabled:
  sysctl.present:
    - name: net.ipv4.ip_forward
    - value: 1

wireguard-config-initially-placed:
  file.managed:
    - name: /etc/wireguard/wg0.conf
    - source: salt://files/wg0.conf.jinja
    - mode: 400
    - template: jinja
    - makedirs: true
    - unless: test -f /etc/wireguard/wg0.conf
    - require:
      - base-packages-installed
      - wireguard-keys-generated

wireguard-interface-running:
  service.running:
    - name: wg-quick@wg0
    - enable: true
    - require:
      - base-packages-installed
      - wireguard-config-initially-placed
      - ip-forwarding-enabled

wgserver-peering-script placed:
  file.managed:
    - name: /root/scripts/peer-master-with-wgserver.sh
    - source: salt://files/peer-master-with-wgserver.sh
    - mode: 750
    - makedirs: true
    - require:
      - wireguard-interface-running