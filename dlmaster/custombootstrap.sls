{% for srvtpye in
  'wireguard',
  'flesh'
%}
custom-{{ srvtpye }}-server-bootstrap-script-placed:
  file.managed:
    - name: /etc/salt/cloud.deploy.d/custom-bootstrap-{{ srvtpye }}-server-salt.sh
    - source: salt://files/custom-bootstrap-{{ srvtpye }}-server-salt.sh
    - unless: grep -q "WARNING. Changes to this file in the salt repo will be overwritten" /etc/salt/cloud.deploy.d/custom-bootstrap-{{ srvtpye }}-server-salt.sh
    - makedirs: True
    - require:
      - dlmaster-master-configured

custom-{{ srvtpye }}-server-bootstrap-script-finalized:
  file.append:
    - name: /etc/salt/cloud.deploy.d/custom-bootstrap-{{ srvtpye }}-server-salt.sh
    - source: /etc/salt/cloud.deploy.d/bootstrap-salt.sh
    - makedirs: True
    - require:
      - custom-{{ srvtpye }}-server-bootstrap-script-placed
{% endfor %}