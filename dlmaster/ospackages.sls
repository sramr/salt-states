{%- set kernelvers = salt["cmd.run"]("uname -r") -%}
base-packages-installed:
  pkg.installed:
    - pkgs:
      - salt-master
      - salt-minion
      - python3-pygit2
      - salt-cloud
      - wireguard
      - wireguard-dkms
      - wireguard-tools
      - salt-api
      - python-cherrypy
      - linux-headers-{{ kernelvers }}
      - nfs-kernel-server
    - require:
      - wireguard-ppa-added

wireguard-ppa-added:
  pkgrepo.managed:
    - ppa: wireguard/wireguard