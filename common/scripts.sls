nextip-script placed:
  file.managed:
    - name: /root/scripts/nextip.sh
    - source: salt://files/nextip.sh
    - mode: 750
    - makedirs: true

combined-nextip-script placed:
  file.managed:
    - name: /root/scripts/combined_nextip.sh
    - source: salt://files/combined_nextip.sh
    - mode: 750
    - makedirs: true

instance-ip-script placed:
  file.managed:
    - name: /root/scripts/peered-ok.sh
    - source: salt://files/peered-ok.sh
    - mode: 750
    - makedirs: true