include:
  - galera-node.ospackages

galera-config-placed:
  file.managed:
    - name: /etc/mysql/conf.d/galera.cnf
    - source: salt://files/galera/galera.cnf
    - makedirs: true
    - template: jinja

change-default-iface-db:
  file.replace:
    - name: /etc/mysql/mariadb.conf.d/50-server.cnf
    - pattern: "^bind-address.*=.*127.0.0.1.*$"
    - repl: "bind-address            = {{ salt['grains.get']('ip4_interfaces:wg0')|first }}"

mask-ufw:
  service.masked:
    - name: ufw

disable-mysql:
  service.dead:
    - name: mysql