#change-default-credentials-proxy:
# file.replace:
#   - name: /etc/proxysql.cnf
#   - pattern: admin_credentials="admin:admin"
#   - repl: admin_credentials="psu:geblubber"
#
#change-default-iface-proxy:
# file.replace:
#   - name: /etc/proxysql.cnf
#   - pattern: mysql_ifaces="0.0.0.0:6032"
#   - repl: mysql_ifaces="127.0.0.1:6032"

proxysql-config-placed:
  file.managed:
    - name: /etc/proxysql.cnf
    - source: salt://files/nextcloud/proxysql.cnf
    - mode: 750
    - makedirs: true
    - require:
      - nextcloud-base-packages-installed

proxysql-running:
  service.running:
    - name: proxysql
    - enable: true
    - require:
      - proxysql-config-placed
    - watch:
      - proxysql-config-placed