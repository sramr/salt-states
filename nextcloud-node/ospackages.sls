nextcloud-base-packages-installed:
  pkg.installed:
    - pkgs:
      - rsync
      - python-pycurl
      - python-apt
      - proxysql
      - apache2
      - php7.4
      - libapache2-mod-php7.4
      - php7.4-common
      - php7.4-mbstring
      - php7.4-xmlrpc
      - php7.4-soap
      - php7.4-ldap
      - php7.4-gd
      - php7.4-xml
      - php7.4-intl
      - php7.4-json
      - php7.4-mysql
      - php7.4-cli
      - php7.4-ldap
      - php7.4-zip
      - php7.4-curl
      - nfs-common
      - python3-mysqldb
    - require:
      - proxysql-repo-added
      - php-ppa-added

proxysql-repo-added:
  pkgrepo.managed:
    - name: deb https://repo.proxysql.com/ProxySQL/proxysql-2.0.x/bionic/ ./
    - file: /etc/apt/sources.list.d/proxysql.list
    - key_url: https://repo.proxysql.com/ProxySQL/repo_pub_key

php-ppa-added:
  pkgrepo.managed:
    - ppa: ondrej/php