{%- set hostname = salt['grains.get']('host') -%}
apache2-running:
  service.running:
    - name: apache2
    - enable: true
    - reload: true
    - watch:
      - nextcloud-vhost-enabled
      - nextcloud-default-vhost-disabled
      - nextcloud-default-vhost-ssl-disabled
    - require:
      - nextcloud-extracted
      - nextcloud-vhost-enabled
      - nextcloud-default-vhost-disabled
      - nextcloud-default-vhost-ssl-disabled

nextcloud-extracted:
  archive.extracted:
    - name: /var/www/
    - source: https://download.nextcloud.com/server/releases/nextcloud-18.0.0.tar.bz2
    - skip_verify: true
    - user: www-data
    - group: www-data
    - keep_source: false
    - require:
      - nextcloud-base-packages-installed
      - proxysql-running

vhost-configured:
  file.managed:
    - name: /etc/apache2/sites-available/nextcloud.conf
    - source: salt://files/nextcloud/nextcloud-vhost.conf
    - mode: 755
    - makedirs: true
    - require:
      - nextcloud-base-packages-installed

nextcloud-vhost-enabled:
  apache_site.enabled:
    - name: nextcloud
    - watch:
      - vhost-configured
    - require:
      - vhost-configured

nextcloud-default-vhost-disabled:
  apache_site.disabled:
    - name: 000-default
    - watch:
      - vhost-configured
    - require:
      - vhost-configured

nextcloud-default-vhost-ssl-disabled:
  apache_site.disabled:
    - name: default-ssl
    - watch:
      - vhost-configured
    - require:
      - vhost-configured

{% for module in
  'rewrite',
  'headers',
  'env',
  'dir',
  'mime'
%}
nextcloud-module-{{ module }}-enabled:
  apache_module.enabled:
    - name: {{ module }}
    - require:
      - vhost-configured
    - watch_in:
      - apache2-running
    - require_in:
      - apache2-running
      - nextcloud-installed
{%- endfor %}

salt-minion-running-and-enabled:
  service.running:
    - name: salt-minion
    - enable: true

datadir-mounted:
  mount.mounted:
    - name: /srv/data/nextcloud
    - device: 192.168.11.2:/srv/data/nfsshare
    - fstype: nfs
    - mkmnt: True
    - persist: True

{% if hostname.endswith('1') is sameas true  %}
first-nextcloud-config-stub-placed:
  file.managed:
    - name: /var/www/nextcloud/config/config.php
    - source: salt://files/nextcloud/nextcloud.conf.jinja
    - template: jinja
    - unless: test -f /var/www/nextcloud/config/config.php
    - user: www-data
    - group: www-data
    - require:
      - nextcloud-extracted
      - datadir-mounted

nextcloud-installed:
  cmd.run:
    - name: php /var/www/nextcloud/occ maintenance:install --database mysql --database-name "nc" --database-user "ncuser" --database-pass "ncuser" --database-host "127.0.0.1:6033" --admin-user ncadmin --admin-pass ncadmin --data-dir /srv/data/nextcloud || rm -f /var/www/nextcloud/config/config.php
    - cwd: /var/www/nextcloud
    - runas: www-data
    - shell: /bin/bash
    - onchanges:
      - first-nextcloud-config-stub-placed
    - require:
      - nextcloud-vhost-enabled
      - datadir-mounted
      - first-nextcloud-config-stub-placed

nextcloud-trusted_domains-configured:
  cmd.run:
    - name: php /var/www/nextcloud/occ config:system:set trusted_domains 0 --update-only --type=string --value={{ salt['grains.get']('ip4_interfaces:eth0')|first }}
    - cwd: /var/www/nextcloud
    - runas: www-data
    - shell: /bin/bash
    - onchanges:
      - nextcloud-installed
    - require:
      - nextcloud-installed

nextcloud-overwrite_cli_url-configured:
  cmd.run:
    - name: php /var/www/nextcloud/occ config:system:set overwrite.cli.url --update-only --type=string --value=http://{{ salt['grains.get']('ip4_interfaces:eth0')|first }}
    - cwd: /var/www/nextcloud
    - runas: www-data
    - shell: /bin/bash
    - onchanges:
      - nextcloud-installed
    - require:
      - nextcloud-trusted_domains-configured
{% else %}
second-nextcloud-config-stub-placed:
  file.managed:
    - name: /var/www/nextcloud/config/config.php
    - source: salt://files/nextcloud/second-nextcloud.conf.jinja
    - template: jinja
    - unless: test -f /var/www/nextcloud/config/config.php
    - user: www-data
    - group: www-data
    - require:
      - nextcloud-extracted
      - datadir-mounted

config-version-updated:
  cmd.run:
    - name: sed -i "s@VERSIONSTRINGHERE@$(sudo -u www-data php /var/www/nextcloud/occ status | grep ' version:' | awk '{ print $NF }')@g" /var/www/nextcloud/config/config.php
    - cwd: /root
    - runas: root
    - shell: /bin/bash
    - onchanges:
      - second-nextcloud-config-stub-placed
    - require:
      - second-nextcloud-config-stub-placed

nextcloud-fake-upgrade-executed:
 cmd.run:
   - name: php /var/www/nextcloud/occ upgrade
   - cwd: /var/www/nextcloud
   - runas: www-data
   - shell: /bin/bash
   - onchanges:
     - config-version-updated
   - require:
     - second-nextcloud-config-stub-placed
     - apache2-running
     - config-version-updated
{% endif %}
apache2-reloaded:
  cmd.run:
    - name: systemctl reload apache2
    - cwd: /root
    - runas: root
    - shell: /bin/bash
    - onchanges:
{% if hostname.endswith('1') is sameas true  %}
      - nextcloud-overwrite_cli_url-configured
{% else %}
      - nextcloud-fake-upgrade-executed
{% endif %}
second-nextcloud-overwrite_cli_url-configured:
  cmd.run:
    - name: php /var/www/nextcloud/occ config:system:set overwrite.cli.url --update-only --type=string --value=http://{{ salt['grains.get']('ip4_interfaces:eth0')|first }}
    - cwd: /var/www/nextcloud
    - runas: www-data
    - shell: /bin/bash
    - onchanges:
      - apache2-reloaded
    - require:
      - apache2-reloaded

second-nextcloud-trusted_domains-configured:
  cmd.run:
    - name: php /var/www/nextcloud/occ config:system:set trusted_domains 0 --update-only --type=string --value={{ salt['grains.get']('ip4_interfaces:eth0')|first }}
    - cwd: /var/www/nextcloud
    - runas: www-data
    - shell: /bin/bash
    - onchanges:
      - apache2-reloaded
    - require:
      - apache2-reloaded
########################################################################################################################
#nextcloud-fake-upgrade-executed:
#  cmd.run:
#    - name: php /var/www/nextcloud/occ upgrade
#    - cwd: /var/www/nextcloud
#    - runas: www-data
#    - shell: /bin/bash
#    - onchanges:
#      - first-nextcloud-config-stub-placed
#    - require:
#      - first-nextcloud-config-stub-placed
#db-created:
#  mysql_database.present:
#    - name: nc
#    - require:
#      - minion-sql-connection-configured
#
#dbuser-created:
#  mysql_user.present:
#    - name: ncuser
#    - host: {% raw %}'%'{% endraw %}
#    - password: ncuser
#    - require:
#      - db-created
#
#dbgrants-performed:
#  mysql_grants.present:
#    - grant: all privileges
#    - database: nc.*
#    - user: ncuser
#    - host: '%'
#    - require:
#      -  dbuser-created
#minion-sql-connection-configured:
#  file.managed:
#    - name: /etc/salt/minion.d/mysql.conf
#    - source: salt://files/nextcloud/minion-sql.conf
#    - template: jinja
#    - require:
#      - nextcloud-extracted